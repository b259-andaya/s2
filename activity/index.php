<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SO2 Activity: Repetition Control Structures and Array Manipulation</title>
</head>

<body>
    <h3>Divisible of Five</h3>
    <?php printDivisibleOfFive(); ?>

    <h2>Array Manipulation</h2>
    <!-- adding an array at the start -->
    <?php array_unshift($students, 'John Smith'); ?>
    <p><?php var_dump($students); ?></p>
    <?php echo count($students); ?>
    <!-- adding an array at the end -->
    <?php array_push($students, 'Jane Smith'); ?>
    <p><?php var_dump($students); ?></p>
    <?php echo count($students); ?>
    <?php array_shift($students); ?>
    <p><?php var_dump($students); ?></p>
    <?php echo count($students); ?>
</body>

</html>